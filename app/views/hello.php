<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Laravel PHP Framework</title>
    <style>
    @import url(//fonts.googleapis.com/css?family=Lato:300,400,700);
    body
    {
        background:#42bdc2;
        color:#fff;
        margin:0;
        font-family:'Lato', sans-serif;
        text-align: center;
    }
    #wrapper {
        margin:40px auto;
        text-align: left;
        width:800px;
    }
    li {
        line-height: 42px;
    }
    li pre {
        display:inline-block;
        background: #ff8a3c;
        padding:8px;
        margin:5px 10px;
        line-height: 23px;
    }
    li.console-return pre{
        background:#3fbf79;
    }
    h2 {
        margin:50px 0;
    }
    </style>
</head>
<body>
    <div id="wrapper">
        <h1>PmCom - Laravel Project Starter</h1>
        <p>Aenean eget nisl sed est tempus tincidunt ac vitae mi. Vivamus egestas interdum neque vel tristique. Aliquam pellentesque eleifend dui a hendrerit. Mauris est lacus, rutrum sed turpis vitae, rutrum sodales nulla. In sed pulvinar dolor. Nulla vel aliquet lorem. Pellentesque vel odio vitae enim viverra varius quis quis nibh. Quisque convallis rhoncus venenatis.</p>

        <h2>Generate SSH Key</h2>
        <ol>
            <li>Log into your Bitbucket account</li>
            <li>Open up terminal</li>
            <li>Type <pre>cd ~/.ssh</pre> followed by <pre>ls -l</pre>. If you see a <pre>id_rsa.pub</pre> file listed, skip to step 7</li>
            <li>If you dont see <pre>id_rsa.pub</pre>, type in <pre>ssh-keygen -t rsa -C "yourbitbucketaccountemail@example.com"</pre></li>
            <li class="console-return">You will probably see something like <pre>Generating public/private rsa key pair.<br /># Enter file in which to save the key (/Users/you/.ssh/id_rsa): [Press enter]</pre>. Just hit enter</li>
            <li>You will then be prompted to enter a Keyphrase, I would recommend using you Bitbucket account password.</li>
            <li>You now need to add the key to your Bitbucket account. In terminal, enter <pre>pbcopy < ~/.ssh/id_rsa.pub</pre>, this will copy your key to clipboard.</li>
            <li>Go to your Bitbucket account, click on your face in the top right, and select <pre>Manage Account</pre></li>
            <li>In the left sidebar menu, click <pre>SSH Keys</pre></li>
            <li>In the label field, enter anything you want, and in the key field, paste your key (cmd+v).</li>
            <li>You should now be able to access the private repos via composer.</li>
        </ol>

        <h2>Setup project</h2>

        <ol>
            <li>
                Open <pre>composer.json</pre> and add or remove the required packages and repositories. The following are currently available:<br />
                <pre>{ "type": "git", "url": "git@bitbucket.org:purposemedia/auth.git" },</pre>
                <pre>{ "type": "git", "url": "git@bitbucket.org:purposemedia/pmassets.git" },</pre>
            </li>
            <li>Locate your project in terminal and run <pre>php composer update</pre></li>
        </ol>

        <h2>Artisan Purpose Media Install</h2>

        <ol>
            <li>Create a new MySQL database.</li>
            <li>Open up terminal and navigate to your cloned PmCom repository e.g. <pre>cd /Users/yourname/get/pmcom/</pre></li>
            <li>Run the following Artisan command <pre>php artisan purposemedia:install</pre></li>
            <li>The above command will setup the projects database details, add packages Service Providers and Aliases, run your packages Migrations and also Seeds the database.</li>
            <li>The above only need running once, If you need to migrate package updates after the first install, you can run <pre>php artisan purposemedia:migrate</pre> and to re-seed (flush the database) run <pre>php artisan purposemedia:seed</pre></li>
        </ol>

    </div>
</body>
</html>
