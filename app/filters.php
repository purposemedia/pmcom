<?php

Route::filter( 'front', function()
{
	$theme = Config::get( 'app.theme' );
	// If set to DEV, the scripts and styles files will be created on every request
	Pmassets::setModeTo( 'DEV' );

	// Scripts
	Pmassets::add( 'jquery-core',			public_path() . "/themes/{$theme}/js/vendor/jquery-1.10.2.min.js", false );
	Pmassets::add( 'jquery',				public_path() . "/themes/{$theme}/js/vendor/jquery-migrate-1.2.1.min.js", 'jquery-core' );
	Pmassets::add( 'modernizr', 			public_path() . "/themes/{$theme}/js/vendor/modernizr-2.6.2.min.js", 'jquery' );
	Pmassets::add( 'selectivizr', 			public_path() . "/themes/{$theme}/js/vendor/selectivizr-min.js", 'jquery' );
	Pmassets::add( 'prismjs', 				public_path() . "/themes/{$theme}/js/vendor/prism.js", 'jquery' );
	Pmassets::add( 'flow-type', 			public_path() . "/themes/{$theme}/plugins/flow-type/flowtype.js", 'jquery' );
	Pmassets::add( 'selectric', 			public_path() . "/themes/{$theme}/plugins/selectric/js/jquery.selectric.js", 'jquery' );
	Pmassets::add( 'owl', 					public_path() . "/themes/{$theme}/plugins/owl-carousel/owl-carousel/owl.carousel.min.js", 'jquery' );
	Pmassets::add( 'motion', 				public_path() . "/themes/{$theme}/plugins/MotionCAPTCHA/jquery.motionCaptcha.0.2.js", 'jquery' );
	Pmassets::add( 'main', 					public_path() . "/themes/{$theme}/js/main.js", 'modernizr' );


	// Styles
	Pmassets::add( 'font-awsome', 			public_path() . "/themes/{$theme}/css/font-awesome/font-awesome.min.css", false );
	Pmassets::add( 'selectric', 			public_path() . "/themes/{$theme}/plugins/selectric/selectric.css", false );
	Pmassets::add( 'owl', 					public_path() . "/themes/{$theme}/plugins/owl-carousel/owl-carousel/owl.carousel.css", false );
	Pmassets::add( 'motion', 				public_path() . "/themes/{$theme}/plugins/MotionCAPTCHA/jquery.motionCaptcha.0.2.css", false );
	Pmassets::add( 'main', 					public_path() . "/themes/{$theme}/css/main_sass.css", false );
});


App::before(function($request)
{

});

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});