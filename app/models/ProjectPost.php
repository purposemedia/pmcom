<?php

use Purposemedia\Posts\Models\Post;
use Purposemedia\Posts\Models\PostParent;

class ProjectPost extends Post
{

	public function children( $paginate = 10 )
	{
		if( ! empty( $this->children ) )
		{
			return $this->children;
		}
		$ids = array();
		// Get Post Parents
		$parents = PostParent::where( 'parent_id', '=', $this->id )->get();
		foreach( $parents as $parent ) :
			$ids[] = $parent->post_id;
		endforeach;

		$children = self::where( 'parent_id', '=', $this->id );
		if( ! empty( $ids ) )
		{
			$children = $children->orWhereIn( 'id', $ids );
		}
		$children  = $children->leftjoin( 'storage_varchars', 'posts.id', '=', 'storage_varchars.post_id' );
		$this->children = $children->orderBy( 'group', 'DESC' )->orderBy( 'storage_varchars.meta_value', 'ASC' )->orderBy( 'name', 'DESC' )->paginate( $paginate );
		return $this->children;
	}

}