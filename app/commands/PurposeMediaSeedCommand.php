<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PurposeMediaSeedCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'purposemedia:seed';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Searches through the Purpose Media Packages and performs db:seed on them.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */

	public function fire()
	{
		$dir = new DirectoryIterator( app_path() . '/../vendor/purposemedia/' );
		foreach ( $dir as $fileinfo )
		{
			if ( ! $fileinfo->isDot() && $fileinfo->isDir() )
			{
				$subDir = new DirectoryIterator( app_path() . '/../vendor/purposemedia/' . $fileinfo->getFilename() . '/src/Purposemedia/' );
				foreach ( $subDir as $subfileinfo )
				{
					if ( ! $subfileinfo->isDot() && $subfileinfo->isDir() )
					{
						$this->line('running db:seed on package: purposemedia/' . $subfileinfo->getFilename() );
						if( class_exists( 'Purposemedia\\' . $subfileinfo->getFilename() . '\\Seeds\\DatabaseSeeder' ) )
						{
							Illuminate\Console\Command::call('db:seed', array('--class' => 'Purposemedia\\' . $subfileinfo->getFilename() . '\\Seeds\\DatabaseSeeder' ) );
						}
						else
						{
							$this->comment('No seeds sown!');
						}
					}
				}
			}
		}
	}


}