<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PurposeMediaProvidersCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'purposemedia:providers';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Searches through the Purpose Media Packages and performs db:migrate on them.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */

	public function fire()
	{
		$aliases = array_reverse( Config::get( 'app.aliases' ) );
		$providers = array_reverse( Config::get( 'app.providers' ) );
		if( ! $provides = $this->getProvidersJSONtoArray() )
		{
			return;
		}
		$appConfigFile = app_path() . '/config/app.php';
		$appConfigContents = file_get_contents( $appConfigFile );
		$appConfigContents = $this->newAliasesToAppConfigFile( $aliases, $provides, $appConfigContents );
		$appConfigContents = $this->newProvidersToAppConfigFile( $providers, $provides, $appConfigContents );

		file_put_contents( $appConfigFile, $appConfigContents );
		$this->info('Aliases, Facades and Service Providers added to config...');
	}

	private function newProvidersToAppConfigFile( $providers, $provides, $appConfigContents )
	{
		$serviceProvider = current( $providers );
		$serviceProviderRegexp = "#(\t*)(['\"]" . preg_quote( $serviceProvider, '#' ) . "['\"])\s*(,?)\n#is";

		if( preg_match( $serviceProviderRegexp, $appConfigContents, $matches ) )
		{
			$tabPrefix = $matches[1];
			$replace = rtrim( $matches[0], "\n" );
			$replaceWith = rtrim( rtrim( $matches[0], "\n" ), "," ) . ",";

			foreach( $provides['providers'] as $serviceProvider )
			{
				if( ! in_array( $serviceProvider, $providers ) )
				{
					$replaceWith .= "\n{$tabPrefix}'{$serviceProvider}',";
					$this->info("{$serviceProvider} service provider added...");
				}
				else
				{
					$this->comment("Unable to add {$serviceProvider} service provider, duplicate.");
				}
			}
			$appConfigContents = str_replace( $replace, $replaceWith, $appConfigContents );
		}
		return $appConfigContents;
	}

	private function newAliasesToAppConfigFile( $aliases, $provides, $appConfigContents )
	{
		$lastAliasOfArray = each( $aliases );
		$lastAlias = $lastAliasOfArray['key'];
		$lastAliasServiceProvider = $lastAliasOfArray['value'];
		$aliasRegexp = "#(\t*)(['\"]" . preg_quote( $lastAlias, '#' ) . "['\"]\s*=>\s*['\"]" . preg_quote( $lastAliasServiceProvider, '#' ) . "['\"])\s*(,?)\n#is";

		if( preg_match( $aliasRegexp, $appConfigContents, $matches ) )
		{
			$tabPrefix = $matches[1];
			$replace = rtrim( $matches[0], "\n" );
			$replaceWith = rtrim( rtrim( $matches[0], "\n" ), "," ) . ",";

			foreach( $provides['aliases'] as $alias => $serviceProvider )
			{
				if( ! isset( $aliases[$alias] ) )
				{
					$replaceWith .= "\n{$tabPrefix}'{$alias}' => '{$serviceProvider}',";
					$this->info( "{$alias} alias with {$serviceProvider} facade added..." );
				}
				else
				{
					$this->comment( "{$alias} alias with {$serviceProvider} facade already exists, not added..." );
				}
			}
			$appConfigContents = str_replace( $replace, $replaceWith, $appConfigContents );
		}
		return $appConfigContents;
	}

	private function getProvidersJSONtoArray( $vendor = 'purposemedia' )
	{
		$path = app_path() . '/../vendor/' . $vendor . '/';
		if( ! file_exists( $path ) )
		{
			$this->error( "Could not locate Purpose Media packages, please run 'php composer.phar update'" );
			return false;
		}
		$dir = new DirectoryIterator( $path );
		$aliases = array();
		$providers = array();
		foreach ( $dir as $fileinfo )
		{
			if ( ! $fileinfo->isDot() && $fileinfo->isDir() )
			{
				$package = $fileinfo->getFilename();
				$provides = base_path() . "/vendor/{$vendor}/{$package}/provides.json";
				if( file_exists( $provides ) )
				{
					if( $providersArray = json_decode( preg_replace("~(\w)\\\(\w)~", "$1\\\\\\\\$2", @file_get_contents( $provides ) ) ) )
					{
						if( isset( $providersArray->providers ) )
						{
							foreach( $providersArray->providers as $provider )
							{
								if( ! in_array( $provider, $providers ) )
								{
									$providers[] = $provider;
								}
							}
						}
						if( isset( $providersArray->aliases ) )
						{
							foreach( $providersArray->aliases as $alias )
							{
								if( isset( $alias->alias ) && isset( $alias->facade ) )
								{
									$aliases[$alias->alias] = $alias->facade;
								}
							}
						}
					}
				}
			}
		}
		return array( 'aliases' => $aliases, 'providers' => $providers );
	}


}