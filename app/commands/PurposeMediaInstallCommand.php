<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PurposeMediaInstallCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'purposemedia:install';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Searches through the Purpose Media Packages and adds alias\' and service providers to the app/config/app.php file.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */

	private function complete()
	{
		$this->info("── ── ── ── ── ── ── ── ── ── ── ── ── ── ── ── ── ──");
		$complete = "── ── ── ── ── ── ── ── ── ── ── ── ── ── ── ── ── ──
── ── ── ── ── ▄▄ ▄▄ ▄▄ ▄▄ ▄▄ ▄▄ ▄▄ ▄▄ ── ── ── ── ──
── ── ── ── ▄▀ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▀▄ ── ── ── ──
── ── ── ▐▌ ▒▒ ██ ██ ▒▒ ▒▒ ▒▒ ▒▒ ██ ██ ▒▒ ▐▌ ── ── ──
── ── ── ▐▌ ▒▒ ██ ██ ▒▒ ▒▒ ▒▒ ▒▒ ██ ██ ▒▒ ▐▌ ── ── ──
▐▌ ▀▄ ── ▐▌ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▐▌ ── ▄▀ ▐▌
▐▌ ▒▒ ▀▄ ▐▌ ▒▒ ▐▌ ▀▄ ▄▀ ▀▄ ▄▀ ▀▄ ▄▀ ▐▌ ▒▒ ▐▌ ▄▀ ▒▒ ▐▌
▐▌ ▒▒ ▒▒ ▐▌ ▒▒ ▐▌ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▐▌ ▒▒ ▐▌ ▒▒ ▒▒ ▐▌
── ▀▄ ▒▒ ▐▌ ▒▒ ▐▌ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▐▌ ▒▒ ▐▌ ▒▒ ▄▀ ──
── ── ▀▄ ▐▌ ▒▒ ▐▌ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▐▌ ▒▒ ▐▌ ▄▀ ── ──
── ── ── ▐▌ ▒▒ ▐▌ ▄▀ ▀▄ ▄▀ ▀▄ ▄▀ ▀▄ ▐▌ ▒▒ ▐▌ ── ── ──
── ── ── ▐▌ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▐▌ ── ── ──
── ── ── ▐▌ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▒▒ ▐▌ ── ── ──";
		$this->error($complete);
		$this->info("── ── ── ── ── ── ── ── ── ── ── ── ── ── ── ── ── ──");
		$this->info("Installation Complete!");
		$this->info("── ── ── ── ── ── ── ── ── ── ── ── ── ── ── ── ── ──");

	}

	public function fire()
	{
		Illuminate\Console\Command::call('purposemedia:mysql');
		if( $this->confirm("Do you want to run migrate:install? [yes|no]", true ) )
		{
			Illuminate\Console\Command::call( 'migrate:install' );
		}
		Illuminate\Console\Command::call( 'purposemedia:providers' );
		Illuminate\Console\Command::call( 'purposemedia:migrate' );
		Illuminate\Console\Command::call( 'purposemedia:seed' );
		$this->complete();

	}

}