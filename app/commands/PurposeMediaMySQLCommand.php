<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Support\Facades\DB;

class PurposeMediaMySQLCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'purposemedia:mysql';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sets up the applications MySQL database details.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */

	public function fire()
	{
		$dbserver = $this->ask('dbserver: ');
		$dbname = $this->ask('dbname: ');
		$dbuser = $this->ask('dbuser: ');
		$dbpass = $this->ask('dbpass: ');

		$this->info("server: '{$dbserver}', name: '{$dbname}', user: '{$dbuser}', pass: '{$dbpass}'");

		if ( $this->confirm("Are these details correct? [yes|no]", true ) )
        {
        	$file = app_path() . '/config/database.php';
        	$mysqlregexp = "#'mysql' => array\([^\)]+\),#is";
			$replace = "'mysql' => array(\n\t\t\t'driver'    => 'mysql',\n\t\t\t'host'      => '{$dbserver}',\n\t\t\t'database'  => '{$dbname}',\n\t\t\t'username'  => '{$dbuser}',\n\t\t\t'password'  => '{$dbpass}',\n\t\t\t'charset'   => 'utf8',\n\t\t\t'collation' => 'utf8_unicode_ci',\n\t\t\t'prefix'    => '',\n\t\t),";

			$dbConfigContents = file_get_contents( $file );
			$configRollback = $dbConfigContents;
			$dbConfigContents = preg_replace( $mysqlregexp, $replace, $dbConfigContents );

			$mysqli = @new \mysqli("$dbserver", "$dbuser", "$dbpass", "$dbname");

			if ( mysqli_connect_errno() )
			{
				$this->error( "Database connect failed!" );
				Illuminate\Console\Command::call('purposemedia:mysql');
			}
			else
			{
				$this->info( "Database connect successful!" );
				if( @file_put_contents( $file, $dbConfigContents ) )
				{
					Config::set( 'database.connections.mysql.host', $dbserver );
					Config::set( 'database.connections.mysql.database', $dbname );
					Config::set( 'database.connections.mysql.username', $dbuser );
					Config::set( 'database.connections.mysql.password', $dbpass );
					DB::reconnect('mysql');
					$this->info( "Details written to file." );
					return true;
				}
				else
				{
					$this->error( "Writing data to file failed!" );
					return false;
				}
			}
        }
        elseif ( $this->confirm("Do you want to start over? [yes|no]", true ) )
        {
        	Illuminate\Console\Command::call('purposemedia:mysql');
        }
        else
        {
        	return false;
        }
	}

}