<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class PurposeMediaMigrateCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'purposemedia:migrate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Searches through the Purpose Media Packages and performs db:migrate on them.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */

	private function is_dir_empty($dir)
	{
		if (!is_readable($dir)) return NULL;
		$handle = opendir($dir);
		while (false !== ($entry = readdir($handle)))
		{
			if ($entry != "." && $entry != "..")
			{
				return FALSE;
			}
		}
		return TRUE;
	}

	public function fire()
	{
		$this->comment( 'creating reminder table...' );
		Illuminate\Console\Command::call( 'auth:reminders-table' );
		$this->comment( 'migrating...' );
		//Illuminate\Console\Command::call( 'migrate' );
		$this->info( 'Auth migtion complete!' );
		$dir = new DirectoryIterator( app_path() . '/../vendor/purposemedia/' );
		foreach ( $dir as $fileinfo )
		{
			if ( ! $fileinfo->isDot() && $fileinfo->isDir() )
			{
				$migrations = app_path() . '/../vendor/purposemedia/' . $fileinfo->getFilename() . '/src/migrations/';
				if( file_exists( $migrations ) && ! $this->is_dir_empty( $migrations ) )
				{
					$this->line('running migrate on package: purposemedia/' . $fileinfo->getFilename() );
					Illuminate\Console\Command::call('migrate', array('--package' => 'purposemedia/' . $fileinfo->getFilename() ) );
				}
			}
		}
	}


}