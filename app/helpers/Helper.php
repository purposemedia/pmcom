<?php

use Purposemedia\Posts\Models\Post;
use Purposemedia\PmComBusinessDirectory\Models\Job;
use Purposemedia\PmComBusinessDirectory\Models\Company;

class Helper
{

	/**
	 * [categoryList description]
	 * @return [type] [description]
	 */

	public static function categoryList()
	{
		$return = array( '0' => '--' );
		foreach( Post::where( 'post_type_id', '=', PMCOMBD_DIRECTORY_CATEGORY )->get() as $post )
		{
			$return[$post->id] = $post->name;
		}
		return $return;
	}

	/**
	 * [vacanciesCategoryList description]
	 * @return [type] [description]
	 */

	public static function vacanciesCategoryList()
	{
		$return = array( '0' => '--' );
		foreach( Post::where( 'post_type_id', '=', PMCOMBD_JOB_CATEGORY )->get() as $post )
		{
			$return[$post->id] = $post->name;
		}
		return $return;
	}

	/**
	 * [post description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */

	public static function post( $id )
	{
		return Post::find( $id );
	}

	/**
	 * [theme_url description]
	 * @param  string $ext [description]
	 * @return [type]      [description]
	 */

	public static function theme_url( $ext = '' )
	{
		return '/themes/' . Config::get( 'app.theme' ) . '/' . $ext;
	}

	/**
	 * [url description]
	 * @return [type] [description]
	 */

	public static function url()
	{
		return Config::get( 'app.url' );
	}

	/**
	 * [latestCompanies description]
	 * @param  integer $num [description]
	 * @return [type]       [description]
	 */

	public static function latestCompanies( $num = 5 )
	{
		return Post::where( 'post_type_id', '=', PMCOMBD_COMPANY )->orderBy( 'created_at', 'DESC' )->take( $num )->get();
	}

	/**
	 * [latestVacancies description]
	 * @param  integer $num [description]
	 * @return [type]       [description]
	 */

	public static function latestVacancies( $num = 5 )
	{
		$latestVacancies = Job::where( 'post_type_id', '=', PMCOMBD_JOB )->orderBy( 'created_at', 'DESC' )->take( $num )->get();
		return $latestVacancies;
	}

	/**
	 * [popularCategories description]
	 * @return [type] [description]
	 */

	public static function popularCategories()
	{
		$results = \DB::select( "SELECT p.id, pc.company_count FROM `posts` AS `p` LEFT JOIN ( SELECT COUNT(id) AS company_count, parent_id AS count_parent_id FROM posts WHERE post_type_id = " . PMCOMBD_COMPANY . " GROUP BY parent_id ) AS `pc` ON `p`.`id` = `pc`.`count_parent_id` WHERE `p`.`post_type_id` = " . PMCOMBD_DIRECTORY_CATEGORY . " ORDER BY `pc`.`company_count` DESC LIMIT 5");
		foreach( $results as $key => $result )
		{
			$results[$key]->post = Post::find( $result->id );
		}
		return $results;
	}

	/**
	 * [directories description]
	 * @return [type] [description]
	 */

	public static function directories()
	{
		return Post::where( 'post_type_id', '=', PMCOMBD_DIRECTORY )->orderBy( 'created_at', 'DESC' )->get();
	}

	/**
	 * [directoryCatCount description]
	 * @param  [type] $directoryID [description]
	 * @return [type]              [description]
	 */

	public static function directoryCatCount( $directoryID )
	{
		return Post::where( 'parent_id', '=', $directoryID )->count();
	}

	/**
	 * [directoryCompCount description]
	 * @param  [type] $directoryID [description]
	 * @return [type]              [description]
	 */

	public static function directoryCompCount( $directoryID )
	{
		$count = 0;
		$categories = Post::where( 'parent_id', '=', $directoryID )->get();
		foreach( $categories as $category )
		{
			$count += Post::where( 'parent_id', '=', $category->id )->get()->count();
		}
		return $count;
	}

	/**
	 * [directoryLatestCompany description]
	 * @param  [type] $directoryID [description]
	 * @return [type]              [description]
	 */

	public static function directoryLatestCompany( $directoryID )
	{
		$catIDs = array();
		$categories = Post::where( 'parent_id', '=', $directoryID )->get();
		foreach( $categories as $category )
		{
			$companies = Post::where( 'parent_id', '=', $category->id )->get();
			foreach( $companies as $company )
			{
				$catIDs[] = $company->id;
			}
		}
		if( ! empty( $catIDs ) )
		{
			return Post::whereIn( 'id', $catIDs )->orderBy( 'created_at', 'DESC')->first();
		}
		return false;
	}

	/**
	 * [categoryGroups description]
	 * @param  [type] $directoryID [description]
	 * @return [type]              [description]
	 */

	public static function categoryGroups( $directoryID )
	{
		$groups = \DB::table( 'posts' )
			->select( \DB::raw( 'SUBSTRING( `name`, 1, 1 ) as alpha, name, id' ) )
			->where( 'parent_id', '=', $directoryID )
			->groupBy( \DB::raw( 'SUBSTRING( name, 0, 2), name' ) )
			->orderBy( \DB::raw( "`alpha`,`name`" ) )
			->get();
		$return = array();
		foreach( $groups as $group )
		{
			$return[$group->alpha][] = Post::find( $group->id );
		}
		return $return;
	}

	/**
	 * [vacancyCategoryGroups description]
	 * @param  [type] $postID [description]
	 * @return [type]         [description]
	 */

	public static function vacancyCategoryGroups( $postID )
	{
		$groups = \DB::table( 'posts' )
			->select( \DB::raw( 'SUBSTRING( `name`, 1, 1 ) as alpha, name, id' ) )
			->where( 'parent_id', '=', $postID )
			->groupBy( \DB::raw( 'SUBSTRING( name, 0, 2), name' ) )
			->orderBy( \DB::raw( "`alpha`,`name`" ) )
			->get();
		$return = array();
		foreach( $groups as $group )
		{
			$return[$group->alpha][] = Post::find( $group->id );
		}
		return $return;
	}

	/**
	 * [getBlogCategory description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */

	public static function getBlogCategory( $id )
	{
		return Post::find( $id );
	}

	/**
	 * [thumbToURL description]
	 * @param  [type] $thumb  [description]
	 * @param  [type] $width  [description]
	 * @param  [type] $height [description]
	 * @param  string $type   [description]
	 * @return [type]         [description]
	 */

	public static function thumbToURL( $thumb, $width, $height, $type = 'false' )
	{
		return "/wideimage/{$width}/{$height}/{$type}/" . str_replace( '[PUBLIC_PATH]/files/', '', $thumb );
	}

	/**
	 * [getCategoryPosts description]
	 * @param  [type]  $categoryID [description]
	 * @param  integer $num        [description]
	 * @return [type]              [description]
	 */

	public static function getCategoryPosts( $categoryID, $num = 5 )
	{
		return Post::where( 'parent_id', '=', $categoryID )->where( 'post_type_id', '=', POST_BLOG_POST )->take( $num )->get();
	}

	/**
	 * [shorten description]
	 * @param  [type]  $str [description]
	 * @param  integer $len [description]
	 * @return [type]       [description]
	 */

	public static function shorten( $str, $len = 100 )
	{
		$str = strip_tags( $str );
		return strlen( $str ) > $len ? trim( substr( $str, 0, $len ) ) . '...' : $str;
	}

	/**
	 * [getSmallFeaturedCats description]
	 * @return [type] [description]
	 */

	public static function getSmallFeaturedCats()
	{
		return Post::whereIn( 'id', array( 17, 18, 19, 20 ) )->get();
	}

	/**
	 * [getLatestBlogPosts description]
	 * @param  integer $num [description]
	 * @return [type]       [description]
	 */

	public static function getLatestBlogPosts( $num = 5 )
	{
		return Post::where( 'post_type_id', '=', POST_BLOG_POST )->orderBy( 'created_at', 'DESC' )->take( $num )->get();
	}

	/**
	 * [getLatestBlogPost description]
	 * @return [type] [description]
	 */

	public static function getLatestBlogPost()
	{
		return Post::where( 'post_type_id', '=', POST_BLOG_POST )->orderBy( 'created_at', 'DESC' )->first();
	}

	/**
	 * [getLatestBlogPostFromCat description]
	 * @param  [type] $catID [description]
	 * @return [type]        [description]
	 */

	public static function getLatestBlogPostFromCat( $catID )
	{
		$post = Post::where( 'post_type_id', '=', POST_BLOG_POST )->where( 'parent_id', '=', $catID )->orderBy( 'created_at', 'DESC' )->first();
		if( is_null( $post ) )
		{
			return false;
		}
		return $post;
	}

	/**
	 * [tweets description]
	 * @param  [type]  $name [description]
	 * @param  integer $num  [description]
	 * @return [type]        [description]
	 */

	public static function tweets( $name, $num = 1 )
	{
		if ( ! Cache::has( 'twitter-feed' ) ) :
			$twitterFeed = Twitter::getUserTimeline( array('screen_name' => $name, 'count' => $num, 'format' => 'json' ) );
			Cache::add( 'twitter-feed', $twitterFeed, 10 );
		else :
			$twitterFeed = Cache::get( 'twitter-feed' );
		endif;
		return json_decode( $twitterFeed );
	}

	/**
	 * [appSetting description]
	 * @param  [type] $name    [description]
	 * @param  [type] $type    [description]
	 * @param  string $wrapper [description]
	 * @return [type]          [description]
	 */

	public static function appSetting( $name, $type, $wrapper = '%s' )
	{
		if( Config::has( 'settings.' . $name ) )
		{
			$value = Config::get( 'settings.' . $name );
		}
		else
		{
			$value = PmComStorage::single( '__' . $name, 0, $type );
		}
		if( $value && (string)$value !== '' )
		{
			return str_replace( '%s', $value, $wrapper );
		}
		return false;
	}

	/**
	 * [getFeaturedVacancies description]
	 * @param  integer $num [description]
	 * @return [type]       [description]
	 */

	public static function getFeaturedVacancies( $num = 5 )
	{
		$rows = Purposemedia\PmComFeatured\Models\FeedPost::where( 'feed_id', '=', 1 )->groupBy( 'post_id' )->get();
		$postIDs = array();
		foreach( $rows as $row ) :
			$postIDs[$row->post_id] = $row->post_id;
		endforeach;
		if( ! empty( $postIDs ) ) :
			return Job::whereIn( 'id', $postIDs )->orderBy( 'created_at', 'DESC' )->take( $num )->get();
		endif;
		return array();
	}

	/**
	 * [ago description]
	 * @param  [type] $time [description]
	 * @return [type]       [description]
	 */

	public static function ago( $time )
	{
	   $periods = array("Second", "Minute", "Hour", "Day", "Week", "Month", "Year", "Decade");
	   $lengths = array("60","60","24","7","4.35","12","10");

	   $now = time();

	       $difference     = $now - $time;
	       $tense         = "ago";

	   for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
	       $difference /= $lengths[$j];
	   }

	   $difference = round($difference);

	   if($difference != 1) {
	       $periods[$j].= "s";
	   }

	   return "$difference $periods[$j]";
	}

	/**
	 * [getCompany description]
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */

	public static function getCompany( $id )
	{
		return Company::find( $id );
	}

	/**
	 * [getHomePageVacancies description]
	 * @param  integer $num [description]
	 * @return [type]       [description]
	 */

	public static function getHomePageVacancies( $num = 5 )
	{
		$rows = Purposemedia\PmComFeatured\Models\FeedPost::where( 'feed_id', '=', 3 )->groupBy( 'post_id' )->get();
		$postIDs = array();
		foreach( $rows as $row ) :
			$postIDs[$row->post_id] = $row->post_id;
		endforeach;
		if( ! empty( $postIDs ) ) :
			return Job::whereIn( 'id', $postIDs )->orderBy( 'created_at', 'DESC' )->take( $num )->get();
		endif;
		return array();
	}

	/**
	 * [getHomePageLatestNews description]
	 * @return [type] [description]
	 */

	public static function getHomePageLatestNews()
	{
		$row = Purposemedia\PmComFeatured\Models\FeedPost::where( 'feed_id', '=', 2 )->groupBy( 'post_id' )->first();
		if( $row )
		{
			return Post::find( $row->post_id );
		}
		return false;
	}

	/**
	 * [getHomePageEvent description]
	 * @return [type] [description]
	 */

	public static function getHomePageEvent()
	{
		$row = Purposemedia\PmComFeatured\Models\FeedPost::where( 'feed_id', '=', 4 )->groupBy( 'post_id' )->first();
		if( $row )
		{
			return Post::find( $row->post_id );
		}
		return false;
	}


}