<?php namespace Project\Repositories;

use Purposemedia\Posts\Repositories\PostHandlerRepository as OriginalPostHandlerRepository;
use Purposemedia\Posts\Models\Post;
use ProjectPost;


class PostRepository extends OriginalPostHandlerRepository
{

	public function validate()
	{
		// $this->uri is an array of the uri segments
		$uri_reverse = array_reverse( $this->URIsliced );
		$slug = $uri_reverse[0];

		// finds all posts ending in the final uri segment and post type
		$valid_posts = Post::return_matching( $slug );

		foreach( $valid_posts as $post )
		{
			// generate a URL from the id of the returned post
			$permalink = Post::get_permalink_from_id( $post->id );

			// if it is identical, this is our entry
			if( strcmp( $permalink, $this->raw_uri ) === 0 )
			{
				// set the post id
				$this->data = $post;

				// Hack of the century!!

				if( $this->data->id == 20 )
				{
					$this->data = ProjectPost::find( $this->data->id );
				}

				// check our process time and return as valid
				return true;
			}
		}
		// no valid posts found
		return false;
	}

}