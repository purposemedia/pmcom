<?php namespace Project\Controllers;

use Purposemedia\Posts\Controllers\FrontController;
use Controller, Input, Redirect, Config, Validator, Mail, Session;

class ContactController extends FrontController {

	public function postIndex()
	{
		$settings = Config::get('settings');
		$v = Validator::make( Input::all(), array(
			'name' 				=> 'required|max:255',
			'company' 			=> 'required|max:255',
			'email_address' 	=> 'required|email',
			'contact_number' 	=> 'required|max:255',
			'query' 			=> 'max:2000'
		));

		if( $v->fails() )
		{
			return Redirect::back()->withInput()->withErrors( $v->messages() );
		}

		Mail::send( 'theme::views.emails.contact', Input::all(), function( $message ) use ( $settings )
		{
			$message->to( $settings['server_delivery_to_email_address'], $settings['application_name'] )->subject( 'Contact Form Submitted' );
		});
		Session::flash( 'success', 'Your message has successfully been delivered.' );
		return Redirect::back();
	}

}
