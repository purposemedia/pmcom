
<div class="container blog-article clearfix">

	<div class="clearfix">

		<div class="content">

			{{ $postInformation->breadcrumb(); }}

			{{ CBRender::renderFront( $postInformation->content ) }}

			@if( $link = $postInformation->editLink() )
				<p><a target="adminpanel" href="{{ $link }}">Edit Post</a></p>
			@endif

		</div>

		<aside class="sidebar">

			<h3>Blog Categories</h3>

			{{ $blog_categories }}

			<h3>Post Archives</h3>

			{{ $blog_archives }}

			<h3>Tags</h3>

			{{ $blog_tags }}

		</aside>

	</div>

	@include( 'theme::views.author' )

</div>