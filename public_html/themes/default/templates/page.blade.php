

<div class="container page clearfix">

	<div class="content">

		{{ CBRender::renderFront( $postInformation->content ) }}

		{{ $blog_reel }}

	</div>

	<aside class="sidebar">

		@if( $blog_categories )

			<h3>Post Categories</h3>

			{{ $blog_categories }}

			<h3>Post Archives</h3>

			{{ $blog_archives }}

			<h3>Post Tags</h3>

			{{ $blog_tags }}

		@endif

	</aside>

</div>