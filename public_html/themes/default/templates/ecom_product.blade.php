
<div class="container blog-article">

	{{ $postInformation->breadcrumb(); }}

	{{ CBRender::renderFront( $postInformation->content ) }}

	@if( $link = $postInformation->editLink() )
		<p><a target="adminpanel" href="{{ $link }}">Edit Post</a></p>
	@endif

	@include( 'theme::views.author' )

</div>