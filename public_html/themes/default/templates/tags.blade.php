
<div class="container archive-tag">

	{{ CBRender::renderFront( $postInformation->content ) }}

	@if( count( $postInformation->children() ) > 0 )


		@foreach( $postInformation->children() as $post )

			<article>
				<h2><a href="{{ $post->permalink() }}">{{ $post->name }}</a></h2>
				<time datetime="{{ $post->created_at }}" pubdate="{{ $post->updated_at }}">{{ date( "d/m/Y @H:i", strtotime( $post->created_at ) ) }}</time>
				{{ $post->post_excerpt }}
			</article>

		@endforeach

		{{ $postInformation->children()->links() }}

	@else

		<p>No posts available</p>

	@endif

</div>
