
<div class="container blog-article clearfix">

	<div class="content">

		{{ $postInformation->breadcrumb(); }}

		{{ CBRender::renderFront( $postInformation->content ) }}

		@if( count( $postInformation->children() ) > 0 )

			@foreach( $postInformation->children() as $post )

				<article>
					<h2><a href="/{{ $post->permalink }}">{{ $post->name }}</a></h2>
					<time datetime="{{ $post->created_at }}" pubdate="{{ $post->updated_at }}">{{ date( "d/m/Y @H:i", strtotime( $post->created_at ) ) }}</time>
					{{ $post->post_excerpt }}
				</article>

			@endforeach

			{{ $postInformation->children()->links() }}

		@else

			<p>No posts available</p>

		@endif

	</div>

	<aside class="sidebar">

		<h3>Blog Categories</h3>

		{{ $blog_categories }}

		<h3>Post Archives</h3>

		{{ $blog_archives }}

		<h3>Tags</h3>

		{{ $blog_tags }}

	</aside>

	@if( $link = $postInformation->editLink() )
		<p><a target="adminpanel" href="{{ $link }}">Edit Post</a></p>
	@endif

</div>