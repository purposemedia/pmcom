

	@if( count( $posts ) > 0 )


		@foreach( $posts as $post )

			<article>
				<h2><a href="{{ $post->permalink }}">{{ $post->name }}</a></h2>
				<time datetime="{{ $post->created_at }}" pubdate="{{ $post->updated_at }}">{{ date( "d/m/Y @H:i", strtotime( $post->created_at ) ) }}</time>
				{{ $post->post_excerpt }}
			</article>

		@endforeach

		{{ $posts->links() }}

	@else

		<p>No posts available</p>

	@endif