
<div class="author">

	<h2>Author: {{ $postInformation->author->first_name }} {{ $postInformation->author->last_name }}</h2>

	<h3>{{ $postInformation->author->position }}</h3>

	<div class="clearfix">
		<div class="cf author-photo">
			<img src="http://www.gravatar.com/avatar/{{ md5( strtolower( $postInformation->author->email ) ) }}.jpg?s=180" width="90" height="90" />
		</div>
		<div class="cf author-bio">
			{{ $postInformation->author->bio }}
		</div>
	</div>

</div>