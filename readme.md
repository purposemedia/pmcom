#PmCom - Purpose Media Content Management System

* Click on "download" on the right hand side.
* Setup a local environment for your project.
* run `composer update` via CLI.
* run `composer dump-autoload` via CLI.
* create a local database for you project.
* run `php artisan purposemedia:install` via CLI and follow the steps until completed.
* run `composer dump-autoload` via CLI.
* DONE!